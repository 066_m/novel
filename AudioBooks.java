package Novel;

public class AudioBooks extends Novel {
    public String audpenname;

    public AudioBooks(String nameNovel, String penname, String audpenname,
        String namePublisher, double price, int quantity){
        super(nameNovel, penname, namePublisher, price, quantity);
        this.audpenname = audpenname;
    }

    @Override
    public void showDescription(){
        System.out.println("Name of Book : " + nameNovel);
        System.out.println("Penname : " + penname);
        System.out.println("Penname of Voice Actor : " + audpenname);
        System.out.println("Name of Publisher : " + namePublisher);
        System.out.printf("Price of Book : %.2f", price);
        System.out.println("\n-----");
    }
}
