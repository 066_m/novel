package Novel;

public class TranslatedFiction extends Novel {
    public String transpenname;

    public TranslatedFiction(String nameNovel, String penname, String transpenname,
        String namePublisher, double price, int quantity){
        super(nameNovel, penname, namePublisher, price, quantity);
        this.transpenname = transpenname;
    }

    @Override
    public void showDescription(){
        System.out.println("Name of Book : " + nameNovel);
        System.out.println("Penname : " + penname);
        System.out.println("Penname of Translator : " + transpenname);
        System.out.println("Name of Publisher : " + namePublisher);
        System.out.printf("Price of Book : %.2f", price);
        System.out.println("\n-----");
    }
}
