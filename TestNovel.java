package Novel;

public class TestNovel {
    public static void main(String[] args) {
        Novel novel = new Novel("nameNovel", "penname", "namePublisher", 1.00, 10);
        novel.showDescription();
        novel.sell(7);
        novel.showDescription(novel.getQuantity());
        novel.addStock(10);
        novel.showDescription(novel.getQuantity()); 

        System.out.println();

        NovelBooks NovI = new NovelBooks("HairPott", "Mookkkoom", "Saloth", 129.00, 150);
        NovI.showDescription();
        NovI.sell(7);
        NovI.showDescription(NovI.getQuantity());
        NovI.addStock(100);
        NovI.showDescription(NovI.getQuantity()); 
        NovI.sell(3);
        NovI.showDescription(NovI.getQuantity());
        NovI.addStock(-1);
        NovI.showDescription(NovI.getQuantity());
        NovI.setQuantity(-22);
        NovI.showDescription(NovI.getQuantity());

        System.out.println();

        NovelBooks NovII = new NovelBooks("Sherlhol", "Koomm", "tholas", 322.00, 99);
        NovII.showDescription();
        NovII.addStock(200);
        NovII.showDescription(NovII.getQuantity());
        NovII.sell(-3);
        NovII.showDescription(NovII.getQuantity());
        NovII.sell(15);
        NovII.showDescription(NovII.getQuantity());
        NovII.setPrice(-333);
        NovII.showDescription(NovII.getQuantity());

        System.out.println();

        TranslatedFiction TrNovI = new TranslatedFiction("Murders", "BBJJ", "wolkey", 
        "tholas", 429.00, 79);
        TrNovI.showDescription();
        TrNovI.sell(77);
        TrNovI.showDescription(TrNovI.getQuantity());
        TrNovI.addStock(200);
        TrNovI.showDescription(TrNovI.getQuantity()); 

        System.out.println();

        AudioBooks AudNovI = new AudioBooks("Poiroto", "YJDD", "MOKO", "Saloth", 
        195.00, 56);
        AudNovI.showDescription();
        AudNovI.addStock(122);
        AudNovI.showDescription(AudNovI.getQuantity());
        AudNovI.sell(35);
        AudNovI.showDescription(AudNovI.getQuantity()); 
        AudNovI.addStock(-122);
        AudNovI.showDescription(AudNovI.getQuantity());
    }
}
